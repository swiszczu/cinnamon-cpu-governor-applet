# Cinnamon CPU Governor Applet

> A simple applet that shows current CPU frequency and allows to change the governor.

## Screenshot:

![screenshot](screenshot.png)
