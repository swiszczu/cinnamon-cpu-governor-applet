const Applet = imports.ui.applet;
const PopupMenu = imports.ui.popupMenu;
const Settings = imports.ui.settings;
const FileUtils = imports.misc.fileUtils;
const Util = imports.misc.util;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const St = imports.gi.St;

const AppID = "cpu-governor@swiszczu";

class CpuApplet extends Applet.TextApplet {
  constructor(orientation, panel_height, instance_id) {
    super(orientation, panel_height, instance_id);

    this.settings = new Settings.AppletSettings(this, AppID, this.instance_id);
    this.settings.bindProperty(Settings.BindingDirection.IN,
      'refresh-interval', 'refreshMillis', this.onUpdate, null);

    //this.refreshMillis = 1000;
    this.freqRegex = /^cpu MHz\W+:\W*([0-9.]*)/;

    this.setAllowedLayout(Applet.AllowedLayout.BOTH);
    this.set_applet_label('GHz');
    this.set_applet_tooltip(_('Click to open Gnome System Monitor'));

    // Get list of available governors
    let governorFile = Gio.File.new_for_path(
      '/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors');
    let [success, governorInfo] = governorFile.load_contents(null);

    let governorText = governorInfo.toString();

    this.governors = [...governorText.split(' ')];

    this.governors = this.governors.map((gov) => {
      return gov.trim();
    });

    this.label = new St.Label({ text: _('Selected CPU governor: ')});
    this.label.style_class = 'popup-menu-item bold';
    this._applet_context_menu.addActor(this.label);

    // Create menu items
    this.governorItems = this.governors.map((gov) => {
      let menuItem = new PopupMenu.PopupIndicatorMenuItem(gov);
      menuItem.setOrnament(PopupMenu.OrnamentType.DOT, false);
      menuItem.connect('activate', this.onGovernorSelected.bind(this, gov));
      this._applet_context_menu.addMenuItem(menuItem);
      return menuItem;
    });

    GLib.free(governorInfo);

    this.onRefreshData();
  }

  onUpdate() {

  }

  onGovernorSelected(governorName) {
    let policyDir = Gio.File.new_for_path('/sys/devices/system/cpu/cpufreq');

    let shContents = "#!/bin/bash\n\n";

    FileUtils.listDirAsync(policyDir, (fileinfo) => {
      fileinfo.forEach((file) => {
        let corePath = GLib.build_filenamev(
          ['/sys/devices/system/cpu/cpufreq', file.get_name()]);
        let govPath = GLib.build_filenamev([corePath, 'scaling_governor']);

        //let govFile = Gio.File.new_for_path(govPath);
        /*let [success, tag] = govFile.replace_contents(governorName, null,
          false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);*/
        shContents += `echo ${governorName} > '${govPath}'\n`;

        GLib.free(corePath);
        GLib.free(govPath);
      });

      let shFile = Gio.File.new_for_path('/tmp/set_governor');
      let [success, tag] = shFile.replace_contents(shContents, null,
        false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);

      Util.spawnCommandLineAsync('pkexec bash /tmp/set_governor');
    });

  }

  onRefreshData(ignoreTimeout) {
    let cpuInfoFile = Gio.File.new_for_path('/proc/cpuinfo');
    let [success, cpuInfo] = cpuInfoFile.load_contents(null);

    let cpuText = cpuInfo.toString();

    let totalFreqMHz = 0;
    let coreCount = 0;

    cpuText.split('\n').forEach((line) => {
      let match = this.freqRegex.exec(line);

      if(match) {
        totalFreqMHz += parseFloat(match[1]);
        coreCount++;
      }
    });

    if(coreCount > 0) {
      this.set_applet_label(this.getFreqString(totalFreqMHz / coreCount));
    }
    else {
      this.set_applet_label('-.-- GHz');
    }

    GLib.free(cpuInfo);

    // Get current governor
    let governorInfoFile = Gio.File.new_for_path(
      '/sys/devices/system/cpu/cpufreq/policy0/scaling_governor');
    let [success2, governorInfo] = governorInfoFile.load_contents(null);

    let governorText = governorInfo.toString().trim();

    for(let i = 0; i < this.governors.length; i++) {
      if(this.governors[i] === governorText) {
        this.governorItems[i].setOrnament(PopupMenu.OrnamentType.DOT, true);
      }
      else {
        this.governorItems[i].setOrnament(PopupMenu.OrnamentType.DOT, false);
      }
    }
    GLib.free(governorInfo);

    if(!ignoreTimeout) {
      setTimeout(this.onRefreshData.bind(this), this.refreshMillis);
    }
  }

  getFreqString(frequency) {
    return (frequency / 1000).toFixed(2) + ' GHz';
  }

  on_applet_clicked() {
    Util.spawnCommandLine('gnome-system-monitor');
  }
}

function main(metadata, orientation, panel_height, instance_id) {
    return new CpuApplet(orientation, panel_height, instance_id);
}
